﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/////////////// - Behaviour of Victory menu - /////////////////////////
public class WinGame : MonoBehaviour
{

    #region - Editor variables - 
    [SerializeField]
    private GameObject winPanel;
    [SerializeField]
    private GameObject hub;
    #endregion

    #region - Unity script lifecycle  -
    // Use this for initialization
    private void Start()
    {
        winPanel.SetActive(false);
    }
    #endregion

    #region - Behavior of buttons - 
    // To return to menu
    public void ReturnToMenu()
    {
        Time.timeScale = 1;
        AppManager.nextScene = 1;//acá sería al mapa de la ciudad o provincia? si es ciudad cada minijuego debe saber a que ciudad pertenese
        LoadLevel.LoadScene();
    }
    #endregion

    #region - Util  - 
    // When player win
    public void FinishGame()
    {
        Time.timeScale = 0;
        winPanel.SetActive(true);
        winPanel.transform.SetAsLastSibling();
        hub.SetActive(false);

    }
    #endregion
}

