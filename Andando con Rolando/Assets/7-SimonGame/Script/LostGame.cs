﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/////////////// - Behaviour of Game over menu - /////////////////////////
public class LostGame : MonoBehaviour
{

    #region - Editor variables - 
    [SerializeField]
    private GameObject endPanel;
    [SerializeField]
    private GameObject hub;
    #endregion

    #region - Unity script lifecycle  - 
    // Use this for initialization
    private void Start()
    {
        endPanel.SetActive(false);
    }
    #endregion

    #region - Behavior of buttons - 
    // To return to menu
    public void ReturnToMenu()
    {
        Time.timeScale = 1;
        AppManager.nextScene = 1;//acá sería al mapa de la ciudad o provincia? si es ciudad cada minijuego debe saber a que ciudad pertenese
        LoadLevel.LoadScene();
    }

    // To restart the game
    public void Restart()
    {
        Time.timeScale = 1;
        //SceneManager.UnloadScene(SceneManager.GetActiveScene());
        LoadLevel.LoadScene();
    }
    #endregion

    #region - Util  - 
    // When player loss
    public void FinishGame()
    {
        Time.timeScale = 0;
        endPanel.SetActive(true);
        endPanel.transform.SetAsLastSibling();
        hub.SetActive(false);

    }
    #endregion
}
