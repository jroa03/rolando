﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/////////////// - Behaviour of Victory menu - /////////////////////////
[RequireComponent(typeof (AudioSource))]
public class SimonBehavior : MonoBehaviour
{
    #region - Editor variables -

    [SerializeField]
    private Button[] buttons;
    [SerializeField]
    private Image[] images;
    [SerializeField]
    private AudioClip[] buttonsSounds;
    [SerializeField]
    private int numberOfSequences;// actualmente se pasa por editor, pero esto puede ser para diferentes dificultades, y setearlo en el appManager
    [SerializeField]
    private LostGame panelGameOver;
    [SerializeField]
    private WinGame panelWin;
    #endregion

    #region - Class variables - 

    private AudioSource myAudioSource;

    private float time = 2; // time to next instruction
    private float time1 = 1; // time to return te original color to the image
    private bool activeCount = false; // active the time1

    private int[][] sequence; // this variable is an array, here saved the instruction sequences
    private bool endSequence = false; // when finish simulation a sequence, it's true
    private bool endUserSequence = true; // when player finish his sequence, it's true
    private int currentSequence = 0;
    private int contSequence = 0; // how many elements of the current sequence are in the array yet
    private int buttonIndex = 0; // the button's index in the current sequence
    private int j = 0; // index to move between the elements of the current sequence
    #endregion

    #region - Unity script lifecycle  -

    // Use this for initialization
    private void Start()
    {
        myAudioSource = GetComponent<AudioSource>();

        Aleatorio(); // inicialize the matrix of sequences

        // disable all buttons
        for (int i = 0; i < 4; i++)
        {
            buttons[i].interactable = false;
        }

	}
	
	// Update is called once per frame
    private void Update()
    {
        time -= Time.deltaTime;

        if (time <= 0 && contSequence >= 0 && endUserSequence)
        {
            time = 2;

            print("Sec: " + currentSequence + ", " + j + ": " + sequence[currentSequence][j]);
            
            Color c = images[sequence[currentSequence][j]].color;
            c.a -= 0.5f;
            images[sequence[currentSequence][j]].color = c;
            buttons[sequence[currentSequence][j]].onClick.Invoke();

            activeCount = true;
        }

        if (activeCount)
        {
            time1 -= Time.deltaTime;
        }
        if (time1 <= 0)
        {
            activeCount = false;
            time1 = 1;

            Color c = images[sequence[currentSequence][j]].color;
            c.a += 0.5f;
            images[sequence[currentSequence][j]].color = c;

            j += 1;
            contSequence -= 1;

            if (contSequence < 0)
                endSequence = true;
        }

        // When the sequence finished, active gameplay
        if (endSequence)
        {
            j = 0;

            // enable all buttons
            for (int i = 0; i < 4; i++)
            {
                buttons[i].interactable = true;
            }

            currentSequence += 1;
            contSequence = currentSequence;

            endUserSequence = false;
            endSequence = false;
            time = 5;
        }
	}
    #endregion

    #region - Behavior of buttons - 

    // When press yellow button 
    public void Yellow()
    {
        myAudioSource.clip = buttonsSounds[0];
        myAudioSource.Play();

        if (!endUserSequence)
        {
            Match(0);
        }
    }

    // When press red button 
    public void Red()
    {
        myAudioSource.clip = buttonsSounds[1];
        myAudioSource.Play();

        if (!endUserSequence)
        {
            Match(1);
        }
    }

    // When press green button 
    public void Green()
    {
        myAudioSource.clip = buttonsSounds[2];
        myAudioSource.Play();

        if (!endUserSequence)
        {
            Match(2);
        }
    }

    // When press blue button 
    public void Blue()
    {
        myAudioSource.clip = buttonsSounds[3];
        myAudioSource.Play();

        if (!endUserSequence)
        {
            Match(3);
        }
    }
    #endregion

    #region  - Util  - 

    // This method integrates the pressed button by the player with the button sequence matrix.
    private void Match(int buttonDown)
    {
        // if the current sequence not finish yet
        if (buttonIndex < currentSequence - 1)
        {
            if (buttonDown == sequence[currentSequence - 1][buttonIndex])
            {
                buttonIndex++;
            }
            // if the player press the wrong button
            else
            {
                print("Perdiste");

                // disable all buttons
                for (int k = 0; k < 4; k++)
                {
                    buttons[k].interactable = false;
                }

                myAudioSource.clip = buttonsSounds[4];
                myAudioSource.Play();

                panelGameOver.FinishGame();
            }            
        }
            // if the current sequence ends and the player presses the last button correctly.
        else if ((buttonIndex == currentSequence - 1) && (buttonDown == sequence[currentSequence - 1][buttonIndex]))
        {
            // disable all buttons
            for (int k = 0; k < 4; k++)
            {
                buttons[k].interactable = false;
            }

            time = 2;
            endUserSequence = true;
            print("Correcto");
            buttonIndex = 0;

            // if current sequence is the last sequence of game, the player wins.
            if (currentSequence - 1 == numberOfSequences - 1)
            {
                print("win");
                panelWin.FinishGame();
            }
        }
            // if player press the wrong button.
        else
        {
            print("Perdiste");
            // disable all buttons
            for (int k = 0; k < 4; k++)
            {
                buttons[k].interactable = false;
            }
            myAudioSource.clip = buttonsSounds[4];
            myAudioSource.Play();
            panelGameOver.FinishGame();
        }
    }

    // This method builds random sequences
    private void Aleatorio()
    {
        int[] sequenceNumbers = new int[numberOfSequences];

        print("Randoms: {");

        for (int i = 0; i < numberOfSequences; i++)
        {
            sequenceNumbers[i] = Random.Range(0, 4);
            print(i + ": " + sequenceNumbers[i]);
        }
        print("}");


        // Esto no anda, porque no puede ser variable la cantidad de filas digamos
        //sequence = new int[7][] { 
        //                            new int[] { sequenceNumbers[0] }, 
        //                            new int[] { sequenceNumbers[0], sequenceNumbers[1] }, 
        //                            new int[] { sequenceNumbers[0], sequenceNumbers[1], sequenceNumbers[2] }, 
        //                            new int[] { sequenceNumbers[0], sequenceNumbers[1], sequenceNumbers[2], sequenceNumbers[3] }, 
        //                            new int[] { sequenceNumbers[0], sequenceNumbers[1], sequenceNumbers[2], sequenceNumbers[3], sequenceNumbers[4] }, 
        //                            new int[] { sequenceNumbers[0], sequenceNumbers[1], sequenceNumbers[2], sequenceNumbers[3], sequenceNumbers[4], sequenceNumbers[5] }, 
        //                            new int[] { sequenceNumbers[0], sequenceNumbers[1], sequenceNumbers[2], sequenceNumbers[3], sequenceNumbers[4], sequenceNumbers[5], sequenceNumbers[6] } 
        //                          };

        
        sequence = new int[numberOfSequences][];
        
        for (int l = 0; l < numberOfSequences; l++){
            sequence [l] = new int[l+1];
            for(int m = 0; m < l+1; m++){  
                sequence [l][m] = sequenceNumbers[m];
            }
        }                       
    }
    #endregion

}
