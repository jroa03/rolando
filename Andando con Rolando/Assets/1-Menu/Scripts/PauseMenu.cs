﻿using UnityEngine;
using System.Collections;

/////////////// - Behaviour of pause menu (canvas) - /////////////////////////

public class PauseMenu : MonoBehaviour
{

    #region - Editor variables - 
    [SerializeField]
    private GameObject pauseMenu;
    [SerializeField]
    private GameObject exitPanel;
    [SerializeField]
    private GameObject settingMenu;
    #endregion

    #region - Class variables - 
    private int optionChosen;
    #endregion

    #region - Unity script lifecycle  - 
    // Use this for initialization
	private void Start () {
        pauseMenu.SetActive(false);
        exitPanel.SetActive(false);
        settingMenu.SetActive(false);
	}
	
	// Update is called once per frame
	private void Update () {

        if (Input.GetButtonDown("Cancel") && !pauseMenu.activeSelf && !settingMenu.activeSelf)
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0;
        }
        else if (Input.GetButtonDown("Cancel") && pauseMenu.activeSelf)
        {
            pauseMenu.SetActive(false);
            exitPanel.SetActive(false);
            Time.timeScale = 1;
        }
	}
    #endregion

    #region - Behavior of buttons - 
    // To return to the game
    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    // To go setting Menu
    public void Settings()
    {
        pauseMenu.SetActive(false);
        settingMenu.SetActive(true);
    }
    
    //To return to city map
    public void ReturnCity()
    {
        optionChosen = 1;
        exitPanel.SetActive(true);
    }

    // To exit Game
    public void ExitGame()
    {
        optionChosen = 2;
        exitPanel.SetActive(true);
    }

    // The behavior of the "Si" button of conffirmation panel.
    public void YesExit()
    {
        switch (optionChosen)
        {
            case 1:
                AppManager.nextScene = 3;//acá sería al mapa de la ciudad o provincia? si es ciudad cada minijuego debe saber a que ciudad pertenese
                LoadLevel.LoadScene();
                break;
            case 2:
                #if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
                #else
                    Application.Quit();
                #endif
                break;
            default:
                print("error");
                break;
        }
    }
    // The behavior of the "No" button of conffirmation panel.
    public void NotExit()
    {
        exitPanel.SetActive(false);
    }

    // To return to pause menu, this method is called from the SettingMenu
    public void ReturnToPauseMenu()
    {
        pauseMenu.SetActive(true);
        settingMenu.SetActive(false);
    }
    #endregion

}
