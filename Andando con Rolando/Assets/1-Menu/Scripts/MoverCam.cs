﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; //sacar solo es para debug

/////////////// - Behaviour of camera (horizontal move) - /////////////////////////
// Encapsulates the behavior for moving the camera
public class MoverCam : MonoBehaviour
{

    #region - Editor variables - 
    [SerializeField]
    private RectTransform image;

    [SerializeField]
    private Text debugText;

    #endregion

    #region - Class variables - 
    // Screen resolution
    private int theScreenWidth;
    private int theScreenHeight;
    private float midle;

    private float originPositionX;
    private float originPositionY;

    //private bool origenX = true;
    //private bool origenY = true;
    // Index for control the camera movement
    //private float i = 58;
    //private int j = 58;
    #endregion

    #region - Unity script lifecycle  - 
    // Use this for initialization
    private void Start()
    {
        theScreenWidth = Screen.width;
        theScreenHeight = Screen.height;

        midle = theScreenWidth / 2;

        originPositionX = image.position.x;
        originPositionY = image.position.y;

        //print("theScreenWidth: " + theScreenWidth + "\n" + "theScreenHeight: " + theScreenHeight);

        debugText.text = theScreenWidth + "\n" + theScreenHeight;

    }

    // Update is called once per frame
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.D))
        {
            debugText.text = Input.mousePosition.x + "\n" + Input.mousePosition.y;
        }
        else if (Input.GetKeyDown(KeyCode.L))
            debugText.text = image.localPosition.x + "\n" + image.localPosition.y;


        if (Input.mousePosition.x >= theScreenWidth-2 && !(image.localPosition.x <= -100))
        {
            //print(Derecha);
            //image.transform.Translate(-Time.deltaTime * 70, 0, 0);
            image.position -= new Vector3(1f, 0, 0);
            //origenX = false;
            //i--;

        }

        else if (Input.mousePosition.x <= 0 && !(image.localPosition.x >= 60))//anda
        {
            //print("Izquierda");
            //image.transform.Translate(Time.deltaTime * 70, 0, 0);
            image.position += new Vector3(1f, 0, 0);
            //origenX = false;
            //print(foto.transform.position.x);
            //j--;
        }

        if (Input.mousePosition.y >= theScreenHeight-2 && !(image.localPosition.y <= -50))
        {
            //print("Arriba");
            //origenY = false;
            //image.transform.Translate(0, -Time.deltaTime * 70, 0);
            image.position -= new Vector3(0, 1f, 0);
        }
        else if (Input.mousePosition.y <= 0 && !(image.localPosition.y >= 155))//anda
        {
            //print("Abajo");
            //origenY = false;
            //image.transform.Translate(0, Time.deltaTime * 70, 0);
            image.position += new Vector3(0, 1f, 0);
        }

        // Requisito de Bruno, que la cámara vuelva a su pocición original
        if ((Input.mousePosition.x < (theScreenWidth - 2) && Input.mousePosition.x > 0) && (Input.mousePosition.y < (theScreenHeight - 2) && Input.mousePosition.y > 0) && ((originPositionX + 5) != image.position.x || (originPositionY + 5) != image.position.y))
        {
            if (((int)originPositionX < (int)image.position.x))
            {
                //image.transform.Translate(-Time.deltaTime * 70, 0, 0);
                image.position -= new Vector3(1f, 0, 0);
            }

            else if (((int)originPositionX > (int)image.position.x))
            {
                //image.transform.Translate(Time.deltaTime * 70, 0, 0);
                image.position += new Vector3(1f, 0, 0);
            }

            if (((int)originPositionY < (int)image.position.y))
            {
                //image.transform.Translate(0, -Time.deltaTime * 70, 0);
                image.position -= new Vector3(0, 1f, 0);
            }

            else if (((int)originPositionY > (int)image.position.y))
            {
                //image.transform.Translate(0, Time.deltaTime * 70, 0); 
                image.position += new Vector3(0, 1f, 0);

            }

        }

        

        //if (originPositionX == image.position.x){
        //    origenX = true;
        //    print(image.position.x.CompareTo(originPositionX));
        //}
        //if (originPositionY == image.position.y)
        //{
        //    origenY = true;
        //}
    }

    public void RestartCam()
    {
        image.position = new Vector2(originPositionX, originPositionY);
    }
    #endregion
}
