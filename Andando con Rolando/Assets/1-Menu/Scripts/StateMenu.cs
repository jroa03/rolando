﻿using UnityEngine;
using System.Collections;

/////////////// - Behaviour of State menu (canvas) - /////////////////////////

public class StateMenu : MonoBehaviour
{

    #region - Editor variables - 
    [SerializeField]
    private GameObject exitPanel; // Exit confirmation box
    #endregion

    #region - Class variables - 
    private bool var = false; // is temporary, is a flag to know when to stop the movement of the camera
    private bool exit = false; // when is true: exit game; when is false: return to main menu 
    #endregion

    #region - Unity script lifecycle  - 
    // Use this for initialization
	private void Start () {
        exitPanel.SetActive(false);
	}
	
	// Update is called once per frame
	private void Update () {

        //cambiar!! esto es temporal (para cargar el mapa de la ciudad sirve)

           

	}
    #endregion

    #region - Behavior of buttons - 
    // This is the button behavior Santa Fe
    public void ApretoSantaFe()
    {
        print("si");
        var = true;
        AppManager.nextScene = 4;
        LoadLevel.LoadSceneAsync(true);
    }

    // To return to main menu
    public void ReturnMenu()
    {
        exit = false;
        exitPanel.SetActive(true);

    }

    // To exit game
    public void ExitGame()
    {
        exit = true;
        exitPanel.SetActive(true);
    }

    // The behavior of the "Si" button of conffirmation panel
    public void YesExit()
    {
        if (exit) { 
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
                    Application.Quit();
#endif
        }
        else
        {
            AppManager.nextScene = 1;
            LoadLevel.LoadSceneAsync();
        }
    }

    // The behavior of the "No" button of conffirmation panel
    public void NotExit()
    {
        exitPanel.SetActive(false);
    }
    #endregion

}
