﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/////////////// - Behaviour of main menu (canvas) - /////////////////////////

// Models the behaviour of main menu
public class MainMenu : MonoBehaviour
{
    #region - Editor variables - 
    [SerializeField]
    private Animator menuCam;
    [SerializeField]
    private Button emptyButton;
    [SerializeField]
    private GameObject newGame;
    [SerializeField]
    private GameObject continueGame;
    [SerializeField]
    private GameObject settings;
    [SerializeField]
    private GameObject credits;
    [SerializeField]
    private GameObject exitGame;
    [SerializeField]
    private Canvas me;
    [SerializeField]
    private GameObject SettingsMenu;
    [SerializeField]
    private GameObject creditsScreen;
    [SerializeField]
    private GameObject exitPanel;
    #endregion

    #region - Class variables - 
    private Animator myAnimator; 
    private Animator settingsMenuAnimator; 
    private Animator creditsScreenAnimator; 

    private Button playButton; 
    private Button continueButton; 
    private Button settingsButton; 
    private Button creditsButton; 
    private Button exitButton; 

    private bool continueButtonWasActivate; // if continue buttom is enable when user press Option is TRUE, else is FALSE

    private MoverCam moveCam;

    #endregion

    #region - Unity script lifecycle  - 
    // Use this for pre-initialization
    private void Awake()
    {
        myAnimator = GetComponent<Animator>();
        settingsMenuAnimator = SettingsMenu.GetComponent<Animator>();
        creditsScreenAnimator = creditsScreen.GetComponent<Animator>();

        SettingsMenu.SetActive(false);
        creditsScreen.SetActive(false);
        exitPanel.SetActive(false);

        moveCam = this.GetComponent<MoverCam>();

        //SceneManager.LoadScene(9, LoadSceneMode.Additive);
        
    }

    // Use this for initialization
    private void Start()
    {
        playButton = newGame.GetComponent<Button>();
        continueButton = continueGame.GetComponent<Button>();
        settingsButton = settings.GetComponent<Button>();
        creditsButton = credits.GetComponent<Button>();
        exitButton = exitGame.GetComponent<Button>();

        emptyButton.Select();

        // If there is saved data, activates the button continue, else it disabled
        if (AppManager.lastLvl == 0)
        {
            continueButton.interactable = false;
        }
        else
        {
            continueButton.interactable = true;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (settingsMenuAnimator.isActiveAndEnabled && settingsMenuAnimator.GetBool("TerminoDesaparecer"))
        {
            SettingsMenu.SetActive(false);
            this.ActiveMainMenuButtons();
        }
        if (creditsScreenAnimator.isActiveAndEnabled && creditsScreenAnimator.GetBool("TerminoDesaparecer"))
        {
            creditsScreen.SetActive(false);
            this.ActiveMainMenuButtons();
        }          

    }
    #endregion

    #region - Behavior of buttons - 
    // To create a new game
    public void NewGame()
    {
        AppManager.nextScene = 3;
        LoadLevel.LoadScene();
    }

    // To continue a geme, Load a game
    public void Continue()
    {
        me.renderMode = RenderMode.WorldSpace;

        //me.renderMode = RenderMode.WorldSpace;
        //AppManager.nextScene = AppManager.lastLvl; //esto no es tan facil, ya que esto me llevaría a la última escena que se vio (directo al mini juego), si se quiere que 
                                                         //se carguen los minijuegos desbloqueados en el mapa de la ciudad hay que agregar más, muchas más, variables en la clase
                                                        //que persiste los datos, incluyendo tal vez un método para guardar escenas y otro para guardar el sonido y gráficos, es decir, dividir el actual guardar.
        moveCam.enabled = false;
        moveCam.RestartCam();
        menuCam.SetBool("Continuar", true);

        continueButton.enabled = false;
        

        

    }

    // To configure sound and graphics
    public void Settings()
    {
        SettingsMenu.SetActive(true);

        myAnimator.Play("desaparecer");
        settingsMenuAnimator.SetBool("Aparecer", true);
        this.DisableMainMenuButtons();
    }

    // To see the credits
    public void Credits()
    {
        creditsScreen.SetActive(true);

        myAnimator.Play("desaparecer");
        creditsScreenAnimator.SetBool("Aparecer", true);

        this.DisableMainMenuButtons();
    }

    // To exit game
    public void Exit()
    {
        exitPanel.SetActive(true);
    }

    // if player select "Si" in the confirmation panel
    public void YesExit()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    // if player select "No" in the confirmation panel
    public void NotExit()
    {
        exitPanel.SetActive(false);
    }

    // To return to main menu
    public void ReturnToMainMenu()
    {
        settingsMenuAnimator.SetBool("Desaparecer", true);
        creditsScreenAnimator.SetBool("Desaparecer", true);
        myAnimator.Play("aparecer");

    }
    #endregion

    #region - Util - 
    // Disable all the buttons in the main menu
    void DisableMainMenuButtons()
    {

        playButton.interactable = false;

        if (continueButton.IsInteractable())
            continueButtonWasActivate = true;
        else
            continueButtonWasActivate = false;
        continueButton.interactable = false;
        settingsButton.interactable = false;
        creditsButton.interactable = false;
        exitButton.interactable = false;
    }

    // Active all the buttons in the main menu
    void ActiveMainMenuButtons()
    {

        playButton.interactable = true;
        if (continueButtonWasActivate)
            continueButton.interactable = true;
        settingsButton.interactable = true;
        creditsButton.interactable = true;
        exitButton.interactable = true;
    }
    #endregion
}

   
