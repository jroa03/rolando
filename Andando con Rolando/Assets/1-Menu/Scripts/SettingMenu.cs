﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/////////////// - Behaviour of setting menu (canvas) - /////////////////////////

public class SettingMenu : MonoBehaviour
{

    #region - Editor variables - 
    [SerializeField]
    private Slider sliderVolume;
    [SerializeField]
    private Slider sliderQuality;
    [SerializeField]
    private MainMenu mainMenu;
    [SerializeField]
    private PauseMenu pauseMenu;
    [SerializeField]
    private float defaultVolume = 0.5f;
    [SerializeField]
    private int defaultGraphicQuality = 3;
    #endregion

    #region - Unity script lifecycle  - 
    // Use this for initialization
	private void Start () {

        // Sets current value of these adjustments
        sliderVolume.value = AppManager.soundVolume;
        sliderQuality.value = AppManager.graphicQuality;
	}
    #endregion

    #region - Behavior of buttons - 
    // When VolumeSlider is modified this method is called
    public void VolumeIsModified()
    {
        AudioListener.volume = sliderVolume.value;
    }

    // When QualitySlider is modified this method is called
    public void QualityIsModified()
    {
        QualitySettings.SetQualityLevel((int)sliderQuality.value);
    }
    
    //To return to menu (main menu or pause menu)
    public void ReturnToMenu()
    {

        if (AppManager.soundVolume != sliderVolume.value || AppManager.graphicQuality != (int)sliderQuality.value)
        {
            // Sets current value of these vars
            AppManager.soundVolume = sliderVolume.value;
            AppManager.graphicQuality = (int)sliderQuality.value;

            FileManager.SaveNewConfigurations(AppManager.graphicQuality, AppManager.soundVolume);
            print("Guardo");
        }

        // To : 1-in main menu; 2-in pause menu (capaz se pueda hacer mejor con una vuelta de tuerca más)
        if (mainMenu != null)
            mainMenu.ReturnToMainMenu();
        else if (pauseMenu != null)
            pauseMenu.ReturnToPauseMenu();

    }

    // Restore the current values to default values
    public void RestoreToDefault()
    {
        // Sets defaults values
        AudioListener.volume = defaultVolume;
        QualitySettings.SetQualityLevel(defaultGraphicQuality);

        // Sets current value of these vars
        AppManager.soundVolume = defaultVolume;
        AppManager.graphicQuality = defaultGraphicQuality;

        // Sets the slader values to current value
        sliderVolume.value = AppManager.soundVolume;
        sliderQuality.value = AppManager.graphicQuality;

    }
    #endregion
}
