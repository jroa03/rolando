﻿using UnityEngine;
using System.Collections;

/////////////// - Behaviour of charge window (canvas) - /////////////////////////

// This is the center of the application, in this class will call all the game scenes.
public class EndCharge : MonoBehaviour
{

    #region - Editor variables - 
    [SerializeField]
    private float chargeTime = 5f; // a liar charge time 
    #endregion

    #region - Unity script lifecycle  - 
    // Update is called once per frame
	private void Update () {

        chargeTime -= Time.deltaTime; // this line simulates the charge time

        if (chargeTime <= 0)
        {
            InvokeNextScene();
        }
	}
    #endregion

    #region - Util - 
    // Load nextScene
    private void InvokeNextScene()
    {
        LoadLevel.LoadSceneAsync();
        this.enabled = false; //no recuerdo por que hago esto.. algún motivo debe de haber.
    }
    #endregion
}
