﻿using UnityEngine;
using System.Collections;

public class MoveMap : MonoBehaviour
{
    public float scrollSpeed = 0.1f;
    public Renderer rend;
    public Mesh mesh;

    private bool bDragging = false;
    private float tiempo = 0.2f;

    void Start()
    {
        rend = GetComponent<Renderer>();

        mesh = this.transform.GetComponent<MeshFilter>().mesh;
    }
    void Update()
    {

        //float offset = Time.deltaTime * scrollSpeed;


        //rend.material.SetTextureOffset("_MainTex", new Vector2(0, scrollSpeed * Time.time));

        if (Input.GetMouseButtonDown(0))
        {
            bDragging = true;
            tiempo = 0.2f;
        }

        if (bDragging)
        {
            if (Input.GetAxis("Mouse Y") < 0 && !(mesh.uv[0].y <= 0.5f))
            {

                scrollSpeed = -0.1f;
            }

            else if (Input.GetAxis("Mouse Y") > 0 && !(mesh.uv[0].y >= 1.1f))
            {
                scrollSpeed = 0.1f;
            }
   
        }

        //print(scrollSpeed);

        if (Input.GetMouseButtonUp(0))
        {
            bDragging = false;

            scrollSpeed = 0;

        }

        if (tiempo <= 0)
        {
            scrollSpeed = 0;
        }
        else if (mesh.uv[0].y <= 0.5f && tiempo > 0)
        {
            scrollSpeed = 0.1f;
            tiempo -= Time.deltaTime;
        }
        else if (mesh.uv[0].y >= 1.1f && tiempo > 0)
        {
            scrollSpeed = -0.1f;
            tiempo -= Time.deltaTime;
        }

        


        print(mesh.uv[0]);

        Vector2[] uvSwap = mesh.uv;

        print(uvSwap.Length);

        for (int b = 0; b < uvSwap.Length; b++)
        {
            uvSwap[b] += new Vector2(0, scrollSpeed * Time.deltaTime);

        }


        mesh.uv = uvSwap;
    

    }
}
