﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLevel {

    public static void LoadScene(bool needCharge = false)
    {

        if (needCharge)
            SceneManager.LoadScene(2);
        else
            SceneManager.LoadScene(AppManager.nextScene);
    }

    public static void LoadSceneAsync(bool needCharge = false)
    {
        if (needCharge)
            SceneManager.LoadSceneAsync(2);
        else
            SceneManager.LoadSceneAsync(AppManager.nextScene);
    }
}
