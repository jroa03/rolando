﻿using UnityEngine;
using System.Collections;

/////////////// - Behaviour of spawn - /////////////////////////
// Models the behaviour of "spawn" (an object that creates other objects)
public class Spawn : MonoBehaviour
{
    #region - Editor variables - 
    [SerializeField]
    private GameObject[] objects;
    [SerializeField]
    private Canvas canvas;
    #endregion

    #region - Class variables -
    private float timeSpawn = 5;
    private Animator myAnimator;
    private int maxVal;
    #endregion

    #region - Unity script lifecycle  - 
    // Use this for initialization
	private void Start () {

        myAnimator = this.GetComponent<Animator>();
        maxVal = objects.Length;
	}
	
	// Update is called once per frame
	private void Update () {
	
        timeSpawn -= Time.deltaTime; // time to spawn any food

        if (timeSpawn <= 0)
        {
            timeSpawn = Random.Range(1, 5);
            myAnimator.speed = Random.Range(1, 10);
            if (objects.Length != 0)
                Spawner(objects[Random.Range(0, maxVal)]);
            else
                print("No hay objetos");
        }
	}
    #endregion

    #region - Util - 
    // It is invoked to generate an object
    private void Spawner(GameObject prefab)
    {
        GameObject Game = (GameObject)Instantiate(prefab, this.transform.position, Quaternion.identity);
        Game.transform.SetParent(canvas.transform);

    }
    #endregion

}
