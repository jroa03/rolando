﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/////////////// - Behaviour of player - /////////////////////////
// Is the player controller
public class GorditoController : MonoBehaviour
{

    #region - Editor variables - 
    [SerializeField]
    private int gordiSpeed = 10;
    [SerializeField]
    private Text  junkFood;
    [SerializeField]
    private Text healthyFood;
    [SerializeField]
    private AudioClip eatFoodSound;
    [SerializeField]
    private AudioClip waghSound;
    [SerializeField]
    private AudioClip lossSound;
    [SerializeField]
    private AudioSource canvasAudioSource;
    [SerializeField]
    private GameOver canvasGameOver;
    [SerializeField]
    private int increaseSpeedOnEat = 2;
    [SerializeField]
    private int reduceSpeedOnEat = 2;
    #endregion

    #region - Class variables -
    private int screenWidth;
    private float myWigth;
    private float myPocition;

    private int junkCount = 0;
    private int healthyCount = 0;
    private AudioSource myAudio;
    #endregion

    #region  - Unity script lifecycle  - 
    // Use this for initialization
	private void Start () {
	    screenWidth = Screen.width;
        print(screenWidth);

        myWigth = ((RectTransform)this.transform).rect.width;
        print(myWigth);

        myAudio = this.GetComponent<AudioSource>();
	}

    // Update is called once per frame
	private void Update () {

        var horizontal = Input.GetAxis("Horizontal");

        myPocition = this.transform.position.x;

        this.transform.Translate(horizontal * gordiSpeed, 0, 0);

        // Player can't pass the screen width
        if ((myWigth / 2) + myPocition >= screenWidth)
        {
            this.transform.Translate(-10, 0, 0);
            //this.transform.position -= this.transform.right*20;
        }
        else if (-(myWigth / 2) + myPocition <= 0)
        {
            this.transform.Translate(+10, 0, 0);
            //this.transform.position -= this.transform.right*20;

        }

        // Special comandos for loss and win
        if (Input.GetKeyDown(KeyCode.I) && Input.GetKeyDown(KeyCode.L))
        {
            canvasAudioSource.clip = lossSound;
            canvasAudioSource.loop = false;
            canvasAudioSource.Play();
            canvasGameOver.FinishGame();
        }
        else if (Input.GetKey(KeyCode.I) && Input.GetKey(KeyCode.W))
        {
            print("you win");

        }

    }

    // Use this when an object enter in a trigger
    private void OnTriggerEnter2D(Collider2D other)
    {
        // if player eating junk food
        if (other.gameObject.tag == "ComidaBasura")
        {
            other.gameObject.SetActive(false);
            Destroy(other.gameObject);
            print("wagh");
            if (gordiSpeed > 0)
                gordiSpeed -= reduceSpeedOnEat;
            junkCount++;
            junkFood.text = junkCount.ToString();
            myAudio.clip = waghSound;
            myAudio.Play();

            // if speed is 0 game over
            if (gordiSpeed == 0)
            {
                canvasAudioSource.clip = lossSound;
                canvasAudioSource.loop = false;
                canvasAudioSource.Play();
                canvasGameOver.FinishGame();
            }

        }
        // if player eating healthy food
        else if (other.gameObject.tag == "ComidaSana")
        {
            other.gameObject.SetActive(false);
            Destroy(other.gameObject);
            print("niam");

            if (gordiSpeed < 9)
                gordiSpeed += increaseSpeedOnEat;
            healthyCount++;
            healthyFood.text = healthyCount.ToString();
            myAudio.clip = eatFoodSound;
            myAudio.Play();
        }
    }
    #endregion
}
