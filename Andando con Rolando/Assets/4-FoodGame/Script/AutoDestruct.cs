﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/////////////// - Behaviour of food - /////////////////////////
// Behaviour to food after fall out
public class AutoDestruct : MonoBehaviour
{

    #region - Editor variables -
    [SerializeField]
    private Sprite splash;
    #endregion

    #region - Class variables -
    private Image myImage;
    private Rigidbody2D myRigid;
    private BoxCollider2D myCollider;
    private AudioSource myAudio;

    private bool sono = false;
    #endregion 

    #region  - Unity script lifecycle  -
    private void Start()
    {
        myImage = GetComponent<Image>();
        myRigid = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<BoxCollider2D>();
        myAudio = GetComponent<AudioSource>();
    }


	// Update is called once per frame
	private void Update () {

        
        if (this.transform.position.y < 115)
        {
            myImage.sprite = splash;
            myRigid.Sleep();
            myCollider.enabled = false;

            if (!sono)
            {
                myAudio.Play();
                sono = true;
            }

            Color c = myImage.color;
            c.a -= Time.deltaTime;
            myImage.color = c;
            print(myImage.color);

            if (myImage.color.a <= 0)
                Destroy(this.gameObject);
        }

    }
    #endregion
}
