﻿using UnityEngine;
using System.Collections;
using System;

/////////////// - Special calss (for percistir game data) - /////////////////////////

// This class is responsible for data persistence. 
[Serializable]
public class GameData {

    public static GameData thisInstance;
    public int graphicQualitycos;
    public float soundVolume;
    public int lvl;
    //public BitArray levelArray;
    //public int playerScore;
    //public ArrayList unblockedBadges;

    // Builder of the class.
    public GameData(int graphicQualitycos, float soundVolume, int lvl)
    {
        this.graphicQualitycos = graphicQualitycos;
        this.soundVolume = soundVolume;
        this.lvl = lvl;
    }
	
}
