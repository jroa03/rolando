﻿using UnityEngine;
using System.Collections;

/////////////// - Special calss (for manage the game) - /////////////////////////

// The first custom class to run. It's the manager of the game.
public static class AppManager {

    /////////////// - Class variables - /////////////////////////
    public static int nextScene = 0;
    public static float soundVolume; // current sound's volume value
    public static int graphicQuality; // current graphic's quality value
    public static int lastLvl; // last level that the gamer played

    /////////////// - Static methods - /////////////////////////
    // Method responsible for initializing the game sound, graphics and other important variables.
    public static void InicialiceGame()
    {
        Debug.Log("Path donde guardo los datos del juego: " + Application.persistentDataPath); //para saber donde guarda las cosas (a nivel de debug)
        Debug.Log("Nombre tarjeta gráfica: " + SystemInfo.graphicsDeviceName);
        Debug.Log("Versión de DirectX: " + SystemInfo.graphicsDeviceVersion);
        Debug.Log("Tamaño memoria gráfica: " + SystemInfo.graphicsMemorySize);
        Debug.Log("Nivel de shader gráfico: " + SystemInfo.graphicsShaderLevel);
        Debug.Log("Sistema operativo: " + SystemInfo.operatingSystem);
        Debug.Log("supportedRenderTargetCount: " + SystemInfo.supportedRenderTargetCount);
        Debug.Log("supportsComputeShaders: " + SystemInfo.supportsComputeShaders);
        Debug.Log("supportsRenderTextures: " + SystemInfo.supportsRenderTextures);
        Debug.Log("Tamaño memoria: " + SystemInfo.systemMemorySize);

        if (FileManager.ExistFile())
        {
            AudioListener.volume = FileManager.savedGames[0].soundVolume;
            QualitySettings.SetQualityLevel((int)FileManager.savedGames[0].graphicQualitycos);
            AppManager.lastLvl = FileManager.savedGames[0].lvl;

            AppManager.soundVolume = AudioListener.volume;
            AppManager.graphicQuality = QualitySettings.GetQualityLevel();
        }
        else
        {
            AppManager.lastLvl = 0;
            // nothing more.
        }
    }
}
