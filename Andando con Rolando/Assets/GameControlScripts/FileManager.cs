﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

/////////////// - Special calss (to manage game data) - /////////////////////////

// This class is responsible for managing the stored data.
// Contains the methods for handling and percistencia Data
public static class FileManager
{
    // Variable to use if you want to have a list of saved games (no es el caso, aunque se utiliza igual)
    public static List<GameData> savedGames = new List<GameData>(); 

    // Load method to initialize the data file if does not exist or if exist the load.
    public static bool ExistFile()
    {
        bool exist;

        if (File.Exists(Application.persistentDataPath + "/savedGames.mat"))
        {
            Load();
            exist = true;
        }
        else
        {
            GameData newGame = new GameData(AppManager.graphicQuality, AppManager.soundVolume, 0);
            GameData.thisInstance = newGame;
            Save();
            exist = false;
        }

        return exist;
    }

    // This method adds a GameData in the list of saved games and updates the file
    private static void Save()
    {
        savedGames.Add(GameData.thisInstance);
        BinaryFormatter binaryformatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.mat"); //you can call it anything you want
        binaryformatter.Serialize(file, FileManager.savedGames);
        file.Close();
    }

    // This method loads the list of saved games from the file and set this in the local list
    private static void Load()
    {
        BinaryFormatter binaryformatter = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/savedGames.mat", FileMode.Open);
        FileManager.savedGames = (List<GameData>)binaryformatter.Deserialize(file);
    }

    // This method recive a optional argument and updates the current instance of GameData and calls Save's method
    public static void SaveNewConfigurations(int graphicQuality = -1, float soundVolume = -1, int lvl = -1)
    {
        if (graphicQuality != -1)
            savedGames[0].graphicQualitycos = graphicQuality;
        if (soundVolume != -1)
            savedGames[0].soundVolume = soundVolume;
        if (lvl != -1)
            savedGames[0].lvl = lvl;
        if (graphicQuality != -1 || soundVolume != -1 || lvl != -1) // always have to save something, if not, why call this method?
            Save();
    }

    //public static int CargarNivel()
    //{
    //    int lastLvl;
    //    lastLvl = savedGames[0].nivel;

    //    return lastLvl;
    //}

}
