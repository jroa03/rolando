﻿using UnityEngine;
using System.Collections;

/////////////// - Behaviour of city menu (canvas) - /////////////////////////

public class CityMenu : MonoBehaviour {

    /////////////// - Editor variables - /////////////////////////
    [SerializeField]
    private GameObject exitPanel;

    /////////////// - Class variables - /////////////////////////
    private int optionChosen; // to know when press "Return Menu", "Return Map" or "Exit Game"

    /////////////// - Unity script lifecycle  - /////////////////////////
	// Use this for initialization
	private void Start () {
        exitPanel.SetActive(false);
	}

    /////////////// - Behavior of buttons - /////////////////////////
    // Este método es el que controla el botón "Hit Me hamburguesa"
    public void Apreto()
    {
        AppManager.nextScene = 6;
        LoadLevel.LoadScene(true);
    }

    // Este método es el que controla el botón "Hit Me simon"
    public void Apreto2()
    {
        AppManager.nextScene = 7;
        LoadLevel.LoadScene(true);
    }
    // Este método es el que controla el botón "Hit Me cubito girando"
    public void Apreto3()
    {
        AppManager.nextScene = 8;
        LoadLevel.LoadScene(true);
    }


    // To return to main menu
    public void ReturnMenu()
    {
        optionChosen = 1;
        exitPanel.SetActive(true);

    }

    // To return to the map
    public void ReturnMap()
    {
        optionChosen = 2;
        exitPanel.SetActive(true);

    }
    
    // To exit game
    public void Exit()
    {
        optionChosen = 3;
        exitPanel.SetActive(true);
    }

    // The behavior of the "Si" button of conffirmation panel.
    public void YesExit()
    {
        switch (optionChosen)
        {
            case 1:
                AppManager.nextScene = 1;
                LoadLevel.LoadScene();
                break;
            case 2:
                AppManager.nextScene = 3;
                LoadLevel.LoadScene();
                break;
            case 3:
                #if UNITY_EDITOR
                    UnityEditor.EditorApplication.isPlaying = false;
                #else
                    Application.Quit();
                #endif
                break;
            default:
                print("error");
                break;
        }

    }
    // The behavior of the "No" button of conffirmation panel.
    public void NotExit()
    {
        exitPanel.SetActive(false);
    }
}
