﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Class responsible for initializing the game. Models the behavior of the initial animation
public class IniGame : MonoBehaviour
{
    [SerializeField]
    private Text debugText;

    #region - Class variables - 
    private Animator myAnimator;
    #endregion

    #region - Unity script lifecycle  - 
    // Use this for pre - initialization
    private void Awake()
    {
        // sets the current volume and graphic quality
        AppManager.soundVolume = AudioListener.volume;
        AppManager.graphicQuality = QualitySettings.GetQualityLevel();

        myAnimator = GetComponent<Animator>();
    }

    // Use this for initialization
    private void Start()
    {
        AppManager.InicialiceGame();
    }

    // Update is called once per frame
    private void Update()
    {

        if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.D))
        {
            myAnimator.speed = 5;
            //AppManager.nextScene = 1;
            //SceneManager.LoadScene(2);
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            Time.timeScale = 0;
            myAnimator.Stop();

            debugText.text = "Path donde guardo los datos del juego: " + Application.persistentDataPath + "\nNombre tarjeta gráfica: " + SystemInfo.graphicsDeviceName +
            "\nVersión de DirectX: " + SystemInfo.graphicsDeviceVersion + "\nTamaño memoria gráfica: " + SystemInfo.graphicsMemorySize + "\nNivel de shader gráfico: " + SystemInfo.graphicsShaderLevel +
            "\nSistema operativo: " + SystemInfo.operatingSystem +
            "\nsupportedRenderTargetCount: " + SystemInfo.supportedRenderTargetCount +
            "\nsupportsComputeShaders: " + SystemInfo.supportsComputeShaders +
            "\nsupportsRenderTextures: " + SystemInfo.supportsRenderTextures +
            "\nTamaño memoria: " + SystemInfo.systemMemorySize;

        }
    }
    #endregion
}
