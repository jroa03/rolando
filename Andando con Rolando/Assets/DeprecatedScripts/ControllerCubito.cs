﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//Controlador del personaje, del Player
[RequireComponent(typeof(Rigidbody))]
public class ControllerCubito : MonoBehaviour
{

    //Propiedades del player -- Modificable Editor

    [SerializeField]
    private float walkSpeed = 6f;

    //Propiedades del player -- No Modificable Editor
    private Rigidbody fisicas;
    private float jumpForce = 3f;
    private float currentSpeed;



    private void Start()
    {

        this.currentSpeed = walkSpeed;
        fisicas = this.GetComponent<Rigidbody>();

    }


    private void Update()
    {

        //Captura de los axis
        var vertical = Input.GetAxis("Vertical");
        var horizontal = Input.GetAxis("Horizontal");
        var jump = Input.GetButtonDown("Jump");

        this.transform.position += this.transform.right * this.walkSpeed * 2f * horizontal * Time.deltaTime;

        this.transform.position += this.transform.forward * vertical * this.currentSpeed * Time.deltaTime * 2f;

        //Saltar
        if (jump)
        {
            Vector3 fwd = transform.TransformDirection(Vector3.down);

            if (Physics.Raycast(transform.position, fwd, 0.6f))
            {
                fisicas.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);

            }
            else
                Debug.Log("Algo");
        }
    }

}
